#!/usr/bin/env python3
import rospy
import os
import signal
import subprocess
import time
from movement_functions import *
import cam_reader as cam
import sim_sender as rtx
import numpy as np
from geometry_msgs.msg import Twist
from std_msgs.msg import Float32MultiArray, Float32

class SmallSizeBridge:
	"""docstring for SmallSizeBridge"""
	def __init__(self):
		print('Small Size Bridge Initialized')
		#------------- Path parameters -----------------
		absFilePath = os.path.abspath(__file__)
		fileDir = os.path.dirname(os.path.abspath(__file__)) 
		pkgManager_path = os.path.join(fileDir, 'pkgManager/pkgManager') 
		#------------- Class parameters ----------------
		self.SIM_FLAG = True    #Dejar siempre en True
		self.NUM_CAMERAS = 4	   #En el simulador se tienen 4 camaras

		self.MAX_ROBOTS = 1     #cantidad de robots por equipo
		self.TEAM = 0 # Blue=0,  Yellow=1    #dejar siempre 0


		# Robot movements
		self.robot_move_abs = np.zeros([2, self.MAX_ROBOTS]) # Vx y Vy en marco global
		self.robot_move_rel = np.zeros([3, self.MAX_ROBOTS]) # Vx, Vy y theta en marco local del robot
		self.kick = np.zeros([1, self.MAX_ROBOTS])
		# Ball
		self.ball = (0, 0)  #coordenada actual (x,y) en marco global de la bola
		self.ball_ant = (0, 0) #coordenada anterior (x,y) en marco global de la bola
		# ------------------ Gr Sim communication -------------- #
		self.pkgManager = subprocess.Popen([pkgManager_path, str(self.NUM_CAMERAS)])
		time.sleep(0.3)

		# inicializar comunicacion
		rtx.init()
		# inicializar el objeto que el que se recibe info de las camaras
		cam.init(self.MAX_ROBOTS)
		done = False
		while not done:
		    print('Waiting for geometry data from grSim...')
		    cam.read()
		    done = cam.ssl_wrapper.HasField('geometry')
		print('Done')

	def velCallBack(self, data):
		self.robot_move_rel[0] = data.linear.x
		self.robot_move_rel[1] = data.linear.y
		self.robot_move_rel[2] = data.angular.z

	def kickCallBack(self, data):
		self.kick[0] = data.data

	def ballFiltering(self):
		self.ball = self.ball_ant
		if len(cam.ballsInfo) > 0:
			dist = 10e6
			for id_ball in range(0, len(cam.ballsInfo)):
				ball_tmp = (cam.ballsInfo[id_ball, 0], cam.ballsInfo[id_ball, 1])
				if dist > np.sqrt((self.ball_ant[0] - ball_tmp[0]) ** 2 + (self.ball_ant[1] - ball_tmp[1]) ** 2):
					self.ball = ball_tmp
					dist = np.sqrt((self.ball_ant[0] - ball_tmp[0]) ** 2 + (self.ball_ant[1] - ball_tmp[1]) ** 2)
			self.ball_ant = self.ball

	def main(self):
		rospy.init_node('small_size_bridge', anonymous = True)
		ball_Position_pub = rospy.Publisher('ball_Position', Float32MultiArray, queue_size = 10)
		robot_Position_pub = rospy.Publisher('robot_Position', Twist, queue_size = 10)
		rospy.Subscriber('robot_move_vel', Twist, self.velCallBack)
		rospy.Subscriber('kick_power', Float32, self.kickCallBack)
		robot_Position_msg = Twist()
		ballPosition_msg = Float32MultiArray()
		rate = rospy.Rate(10)
		print('Ready to publish and receive data')
		while not rospy.is_shutdown():
			cam.read()
			robot_Position_msg.linear.x = cam.robotsInfo[self.TEAM][0][0]/1000
			robot_Position_msg.linear.y = cam.robotsInfo[self.TEAM][0][1]/1000
			robot_Position_msg.linear.z = 0
			robot_Position_msg.angular.x = 0
			robot_Position_msg.angular.y = 0
			robot_Position_msg.angular.z = cam.robotsInfo[self.TEAM][0][2]
			robot_Position_pub.publish(robot_Position_msg)
			self.ballFiltering()
			self.ball = [i/1000 for i in self.ball]
			ballPosition_msg.data = self.ball
			ball_Position_pub.publish(ballPosition_msg)
			rate.sleep()

			# crear paquete de velocidad del robot (en marco local) para enviar al simulador
			rtx.package_init(self.MAX_ROBOTS)
			rtx.vx = self.robot_move_rel[0] #vel lineal en x
			rtx.vy = self.robot_move_rel[1] #vel lineal en y
			rtx.vw = self.robot_move_rel[2] #vel angular
			rtx.kx = self.kick[0]
			# enviar el paquete
			rtx.send()

		# detener el robot
		self.robot_move_rel[0] = 0.0
		self.robot_move_rel[1] = 0.0
		self.robot_move_rel[2] = 0.0
		rtx.package_init(self.MAX_ROBOTS)
		rtx.vx = self.robot_move_rel[0]
		rtx.vy = self.robot_move_rel[1]
		rtx.vw = self.robot_move_rel[2]
		rtx.send()

		# ------------- HASTA AQUI ----------------------

		#  NO MODIFICAR
		print('Ending process...')
		cam.close()
		os.killpg(os.getpgid(self.pkgManager.pid), signal.SIGTERM)
		#--------------------


if __name__ == '__main__':
	small_size = SmallSizeBridge()
	small_size.main()