## Small size Bridge
Para correr el siguiente paquete de ros realice los siguientes pasos de manera previa: 
1. Instalar grSim en el directorio de su preferencia. Para ello, siga las instrucciones dadas en: 

https://github.com/RoboCup-SSL/grSim/blob/master/INSTALL.md

2. Clone este repositorio en el workspace de su preferencia: 

	git clone https://gitlab.com/jjgarcia10/small-size-bridge.git

3. Instalar las siguientes dependencias: 

	- pip3 install protobuf
	- Hace falta más(?)


4. Compile y establezca las variables de entorno del workspace en donde tiene el paquete: 

	- catkin_make
	- source devel/setup.bash

5. Ejecute grSim: 

	- En la carpeta clonada: 
		- cd bin
		- ./grSim

	- En la pestaña _Communication_, cambiar _Vision multicast port_ a *10006*
	- En la pestaña _Geometry/Game_ cambiar _Robots Count_ a 1

6. En otra terminal adicional ejecutar el nodo: 

	- rosrun small_size_wrapper small_size_bridge.py